//
//  AppDelegate.swift
//  BBCSplash
//
//  Created by Håvard Fossli on 12.10.2018.
//  Copyright © 2018 BBC. All rights reserved.
//

import UIKit

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?


    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplicationLaunchOptionsKey: Any]?) -> Bool {
        
        guard let window = self.window else {
            fatalError("Assuming window is present")
        }
        
        let splashView = SplashView(frame: .zero)
        splashView.debug()
        splashView.addToWindow(window)
        
        DispatchQueue.main.asyncAfter(deadline: .now() + 2.0) {
            splashView.revealAndRemove()
        }
        
        return true
    }

}

