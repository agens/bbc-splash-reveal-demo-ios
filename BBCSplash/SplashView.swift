//
//  SplashView.swift
//  BBCSplash
//
//  Created by Håvard Fossli on 12.10.2018.
//  Copyright © 2018 BBC. All rights reserved.
//

import Foundation
import UIKit
import Lottie

class SplashView: UIView {
    
    /*
     
     Booting the app is known to be CPU intensive and we don't want the splash screen
     to add to that. This class is coded trying to utilize the GPU as much as possible.
     
     If you inspect the app with "Color Off-screen Rendered" on it will show which views
     is being rendered on the CPU (the Lottie animation) and which views is largely being
     rendered on GPU.
     
     This is mostly achieved by using an overlay which only causes the GPU to blend layers
     instead of creating a mask view. Mask views causes offscreen rendering which means
     the layers in question needs to be rendered and flattened on CPU and we don't want
     that while booting. You can get pretty far just by using views layered on top of each other.
     
     */
    
    struct Config {
        var heightFactor: CGFloat
        var initialOffset: CGFloat
        var finalOffset: CGFloat
    }
    
    enum State {
        case loading
        case final
    }
    
    struct BBCColor {
        static let orange = UIColor(red: 255.0/255.0, green: 73.0/255.0, blue: 0.0/255.0, alpha: 1.0)
    }
    
    let text = UIImageView()
    let logo = LOTAnimationView(name: "splash-logo-lottie")
    let overlay = OverlayView(frame: .zero)
    var revealed = false
    private var debugging = false
    
    func debug() {
        #if !DEBUG
            print("Tried to activate debug-mode in \(type(of: self)). Action overrided because of lack of DEBUG-definition")
            return
        #endif
        debugging = true
        
        let tapRecognizer = UITapGestureRecognizer(target: self, action: #selector(self.didTap))
        addGestureRecognizer(tapRecognizer)
        
        let twoFingerTapRecognizer = UITapGestureRecognizer(target: self, action: #selector(self.didTwoFingerTap))
        twoFingerTapRecognizer.numberOfTouchesRequired = 2
        addGestureRecognizer(twoFingerTapRecognizer)
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        setup()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        setup()
    }
    
    func setup() {
        autoresizingMask = [.flexibleWidth, .flexibleHeight]
        layer.allowsGroupOpacity = false
        clipsToBounds = false
        
        overlay.backgroundColor = .clear
        addSubview(overlay)
        
        text.layer.allowsGroupOpacity = false
        text.image = UIImage(named: "splash-bbc-blocks.png")
        text.clipsToBounds = false
        text.contentMode = .topLeft
        text.backgroundColor = .clear
        addSubview(text)
        
        logo.layer.allowsGroupOpacity = false
        logo.backgroundColor = .clear
        logo.loopAnimation = true
        logo.contentMode = .scaleAspectFit
        logo.play()
        addSubview(logo)
        
        backgroundColor = .clear
        
        NotificationCenter.default.addObserver(forName: .UIApplicationWillChangeStatusBarFrame, object: nil, queue: .main) {  [weak self] (notification) in
            self?.resetRootViewControllerTransform()
        }
    }
    
    var config: Config {
        let aspect = bounds.width / bounds.height
        let tall: CGFloat =  375.0 / 812.0 // e.g. portrait iphone x and friends
        let wide: CGFloat =  812.0 / 375.0 // e.g. landscape iphone x and friends
        let progress = inverseLerp(min: tall, max: wide, value: aspect)
        
        return .init(heightFactor: lerp(min: 2.2, max: 8.0, progress: progress),
                     initialOffset: lerp(min: 200, max: 250, progress: progress),
                     finalOffset: lerp(min: -30, max: -20, progress: progress))
    }
    
    var state = State.loading
    
    override func hitTest(_ point: CGPoint, with event: UIEvent?) -> UIView? {
        // Capture any touch events while presenting splash screen
        // to avoid any accidental actions
        return self
    }
    
    @objc func didTap(_ sender: UITapGestureRecognizer) {
        if !revealed {
            startAnimation()
        } else {
            prepareForAnim()
        }
    }
    
    @objc func didTwoFingerTap(_ sender: UITapGestureRecognizer) {
        if window?.layer.sublayerTransform.m11 != 1.0 {
            window?.layer.sublayerTransform = CATransform3DIdentity
        } else {
            window?.layer.sublayerTransform = CATransform3DMakeScale(0.5, 0.5, 1.0)
        }
    }
    
    func startAnimation(_ onDone: @escaping () -> () = {}) {
        revealed = true
        UIView.animateKeyframes(withDuration: 1.0, delay: 0.0, options: [], animations: {
            UIView.addKeyframe(withRelativeStartTime: 0.0, relativeDuration: 1.0, animations: {
                self.overlay.state = .init(scale: 1.0, horisontalOffset: self.config.finalOffset)
            })
            UIView.addKeyframe(withRelativeStartTime: 0.50, relativeDuration: 0.02, animations: {
                self.logo.alpha = 0.0
                self.text.alpha = 0.0
            })
            UIView.addKeyframe(withRelativeStartTime: 0.99, relativeDuration: 0.01, animations: {
                if !self.debugging {
                    self.overlay.alpha = 0.0
                }
            })
            UIView.addKeyframe(withRelativeStartTime: 0.0, relativeDuration: 1.0, animations: {
                self.window?.rootViewController?.view.transform = CGAffineTransform.identity
            })
        }, completion: { (success) in
            onDone()
        })
    }
    
    func prepareForAnim() {
        revealed = false
        overlay.state = .init(scale: 0.0, horisontalOffset: config.initialOffset)
        logo.alpha = 1.0
        text.alpha = 1.0
        overlay.alpha = debugging ? 0.9 : 1.0
        overlay.alpha = 1.0
        window?.rootViewController?.view.transform = CGAffineTransform.identity.scaledBy(x: 0.95, y: 0.95)
    }
    
    func addToWindow(_ window: UIWindow) {
        window.makeKeyAndVisible()
        window.addSubview(self)
        layoutNow()
        prepareForAnim()
    }
    
    func resetRootViewControllerTransform() {
        // When device orientation changes the application will most likely
        // just change the frame of the root view controller. Changing the
        // frame of a view while a transform is applied results in unknown
        // behavior. Therefore it is better to remove the scale effect in
        // this edge case (rotate while showing splash)
        self.window?.rootViewController?.view.transform = CGAffineTransform.identity
    }
    
    func revealAndRemove() {
        startAnimation() {
            if !self.debugging {
                self.removeFromSuperview()
            }
        }
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        layoutNow()
    }
    
    func layoutNow() {
        
        if let rootView = self.window?.rootViewController?.view {
            bounds = rootView.bounds
            center = rootView.center
        }
        
        let size = 64.5
        let midPoint = CGPoint(x: bounds.width / 2, y: bounds.height / 2)
        
        overlay.center = CGPoint(x: midPoint.x, y: midPoint.y)
        overlay.bounds = CGRect(x: 0, y: 0, width: bounds.width * 2.5, height: bounds.height * config.heightFactor)
        overlay.layer.sublayerTransform = CATransform3DMakeRotation(CGFloat.pi * (22.0/180.0), 0, 0, 1)
        
        text.frame.size = CGSize(width: 100, height: 13)
        text.center = CGPoint(x: midPoint.x, y: midPoint.y - 31)
        
        logo.frame.size = CGSize(width: size, height: size)
        logo.center = CGPoint(x: midPoint.x + 1, y: midPoint.y + 9.5)
    }

    func lerp(min: CGFloat, max: CGFloat, progress: CGFloat) -> CGFloat {
        return min + (progress * (max - min))
    }
    
    func inverseLerp(min: CGFloat, max: CGFloat, value: CGFloat) -> CGFloat {
        let range = max - min
        return range == 0.0 ? 0.0 : (value - min) / range
    }
    
    
    
    class OverlayView: UIView {
        
        /*
         
         In this view 4 boxes will be arranged to fill the view like this
         
         |-------------------------------------------------
         |                |  (solidTop)  |                |
         |  (solidLeft)   |--------------|  (solidRight)  |
         |                |              |                |
         |                | (imageView)  |                |
         |                |              |                |
         |----------------|--------------|----------------|
         
         The state.scale property determines the height and width of the imageView and
         surrounding boxes will shrink/grow and move accordingly.
         
         The state.horisontalOffset property moves the imageView horisontally and
         surrounding boxes will shrink/grow and move accordingly.
         
         */
        
        let imageView = UIImageView()
        let solidLeft = OverlayView.createSolidColoredView()
        let solidTop = OverlayView.createSolidColoredView()
        let solidRight = OverlayView.createSolidColoredView()
        
        static func createSolidColoredView() -> UIView {
            let view = UIView()
            view.backgroundColor = BBCColor.orange
            view.clipsToBounds = false
            return view
        }
        
        override init(frame: CGRect) {
            super.init(frame: frame)
            setup()
        }
        
        required init?(coder aDecoder: NSCoder) {
            super.init(coder: aDecoder)
            setup()
        }
        
        struct State {
            var scale: CGFloat
            var horisontalOffset: CGFloat
        }
        
        var state = State(scale: 0.0, horisontalOffset: 0.0) {
            didSet {
                layoutNow()
            }
        }
        
        func setup() {
            imageView.image = UIImage(named: "splash-brush.png")
            imageView.contentMode = .scaleToFill
            imageView.backgroundColor = .clear
            
            layer.allowsGroupOpacity = false
            clipsToBounds = false
            
            addSubview(solidLeft)
            addSubview(solidTop)
            addSubview(solidRight)
            addSubview(imageView)
        }
        
        override func layoutSubviews() {
            super.layoutSubviews()
            layoutNow()
        }
        
        func layoutNow() {
            
            let contentSize = imageView.image?.size ?? bounds.size
            let aspectRatio = contentSize.width / contentSize.height
            
            let imageHeight = bounds.height * state.scale
            let imageWidth = imageHeight * aspectRatio
            let sidePadWidth = (bounds.width - imageWidth) / 2
            
            solidLeft.frame = CGRect(x: 0, y: 0, width: sidePadWidth + state.horisontalOffset, height: bounds.height)
            solidTop.frame = CGRect(x: sidePadWidth + state.horisontalOffset, y: 0, width: imageWidth, height: bounds.height - imageHeight)
            solidRight.frame = CGRect(x: sidePadWidth + imageWidth + state.horisontalOffset, y: 0, width: sidePadWidth - state.horisontalOffset, height: bounds.height)
            imageView.frame = CGRect(x: sidePadWidth + state.horisontalOffset, y: bounds.height - imageHeight, width: imageWidth, height: imageHeight)
        }
    }

}
