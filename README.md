BBCSplash
========


## Install

1. Copy the folder `Splash-assets` to your project
2. Copy `SplashView.swift` to your project
3. Add this to app delegate

```swift
    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplicationLaunchOptionsKey: Any]?) -> Bool {

        guard let window = self.window else {
            fatalError("Assuming window is present")
        }

        let splashView = SplashView(frame: .zero)
        splashView.addToWindow(window)

        DispatchQueue.main.asyncAfter(deadline: .now() + 2.0) {
            splashView.revealAndRemove()
        }

        return true
    }
```



## Debugging

Make debug tools available
- two finger tap to zoom in/out to understand the animation better
- one finger tap to reset and restart animation

```swift
    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplicationLaunchOptionsKey: Any]?) -> Bool {

        guard let window = self.window else {
            fatalError("Assuming window is present")
        }

        let splashView = SplashView(frame: .zero)
>>>>>   splashView.debug()
        splashView.addToWindow(window)

        DispatchQueue.main.asyncAfter(deadline: .now() + 2.0) {
            splashView.revealAndRemove()
        }

        return true
    }
```



## Questions about implementation?

Call Håvard Fossli +47 476 50 143, send email to hfossli@gmail.com or find me on slack as hfossli.
